var goodmeeting = angular.module('goodmeeting', []);

function meetingController($scope, $http) {
    $scope.formData = {};
    
    var searchString = window.location.search,
        id;
    
    if (searchString.charAt(0) == '?') {
        var eqPos = searchString.indexOf('=');
        
        var searchTerm = searchString.substr(1, (eqPos - 1));
        if (searchTerm == 'meetingid') {
            id = searchString.substr(eqPos + 1);
        }
    }
    
    setupRating = function(data) {
        $('input[name=rating]').each(function (idx, item) {
           if (item.value == data.rating) {
               $(item).prop('checked', true);
           }
        });   
    }

    // when landing on the page, get the meeting
    $http.get('/api/meeting/' + id)
        .success(function(data) {
            $scope.meeting = data;
            console.log(data);
        
            setupRating(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

    // when submitting the add form, send the rating to the node API
    $scope.updateRating = function() {
        $http.post('/api/meetings/updateRating/' + id, $scope.formData)
            .success(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.meetings = data;
                console.log(data);
            
                setupRating(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
    
    $scope.ratingSetup = {
        ratings: [{
                id: 1,
                text: "0"
            }, {
                id: 2,
                text: "1"
            }, {
                id: 3,
                text: "2"
            }, {
                id: 4,
                text: "3"
            }, {
                id: 5,
                text: "4"
            }, {
                id: 6,
                text: "5"
            }]
    };   
}