// set up ========================
    var express  = require('express');
    var app      = express();                               // create our app w/ express
    var mongoose = require('mongoose');                     // mongoose for mongodb
    var morgan = require('morgan');             // log requests to the console (express4)
    var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
    var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

    // configuration =================

    mongoose.connect('mongodb://127.0.0.1:27017');

    app.use(express.static(__dirname + '/build'));                  // set the static files location /public/img will be /img for users
    app.use(morgan('dev'));                                         // log every request to the console
    app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
    app.use(bodyParser.json());                                     // parse application/json
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    app.use(methodOverride());

    // listen (start app with node server.js) ======================================
    app.listen(8080);
    console.log("App listening on port 8080");
// define model =================
    var Meeting = mongoose.model('Meeting', {
        name : String,
        description : String,
        rating: Number
    });
// api routes ======================================================================

    //app.get('/', function(req, res) {
    //    res.sendfile('build/html/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    //});

    //app.get('/corejs', function(req, res) {
    //    res.sendfile('build/html/js/core.js'); // load the single view file (angular will handle the page changes on the front-end)
    //});

    app.use(express.static('build/html'));
    app.use(express.static('build/html/js'));

// api routes ======================================================================

    // api ---------------------------------------------------------------------
    // get all meetings
    app.get('/api/meetings', function(req, res) {

        // use mongoose to get all meetings in the database
        Meeting.find(function(err, meetings) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)

            res.json(meetings); // return all meetings in JSON format
        });
    });

    // get a meeting
    app.get('/api/meeting/:meeting_id', function(req, res) {
        Meeting.findOne({
            _id : req.params.meeting_id
        }, function(err, meeting) {
            if (err)
                res.send(err);

            res.json(meeting);
        });
    });

    // update the rating on a meeting
    app.post('/api/meetings/updateRating/:meeting_id', function(req, res) {
        Meeting.findById(req.params.meeting_id, function (err, meeting) {
            if (err)
                return handleError(err);

            meeting.rating = req.body.rating;
            meeting.save(function (err, updatedMeeting) {
                if (err)
                    return handleError(err);
                
                res.send(updatedMeeting);
            });
        });
    });

    // create meeting and send back all meetings after creation
    app.post('/api/meetings', function(req, res) {
            console.log(req);
        // create a meetings, information comes from AJAX request from Angular
        Meeting.create({
            name : req.body.name,
            done : false
        }, function(err, meeting) {
            if (err)
                res.send(err);

            // get and return all the meetings after you create another
            Meeting.find(function(err, meetings) {
                if (err)
                    res.send(err)
                res.json(meetings);
            });
        });

    });

    // delete a meeting
    app.delete('/api/meetings/:meeting_id', function(req, res) {
        Meeting.remove({
            _id : req.params.meeting_id
        }, function(err, meeting) {
            if (err)
                res.send(err);

            // get and return all the meetings after you create another
            Meeting.find(function(err, meetings) {
                if (err)
                    res.send(err)
                res.json(meetings);
            });
        });
    });